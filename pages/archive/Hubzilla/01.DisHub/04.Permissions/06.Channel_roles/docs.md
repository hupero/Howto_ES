---
title: "Hubzilla: Channel roles permissions table"
published: true
taxonomy:
    category:
        - docs
    tags:
        - Hubzilla
        - DisHub
visible: true
page-toc:
    active: false
---


![](permissions_table.png?lightbox=1024)
