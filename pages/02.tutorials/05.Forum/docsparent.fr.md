---
title: 'Forum'
subtitle: "Les bases de Discourse"
icon: fa-stack-exchange
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - Forum
        - Discourse
page-toc:
    active: false
---

![](/home/icons/discourse.png)

**Discourse** est un forum Internet Open Source et un logiciel de gestion de listes de diffusion.
<br>
<br>
