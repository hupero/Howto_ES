---
title: "Email Clients"
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

## Client di posta Desktop
- [**Multipiattaforma**](desktop)
- [**GNOME** integrazione desktop](desktop/gnome-desktop-integration)
- [**KDE** integrazioen desktop](desktop/kde-desktop-integration)


## Client di posta Mobile
- [**Android: FairEmail**](mobile/fairemail)
- [**Android: K9**](mobile/k9)
- [**SailfishOS: Mail App**](mobile/sailfishos)
- [**iOS: Mail App**](mobile/ios)
