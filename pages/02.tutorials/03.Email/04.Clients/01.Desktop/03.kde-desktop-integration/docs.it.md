---
title: GNU/Linux: KDE Desktop Integration
published: true
visible: false
updated:
        last_modified: "April 2019"
        app: KDE Plasma
        app_version: 5.15.2 For Manjaro Linux
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
  active: false
---

![](en/kde.png)

**KMail** è il componente e-mail di Kontact, il gestore integrato delle informazioni personali dall'ambiente desktop KDE.

Il vantaggio dell'utilizzo di KMail è che si integra con il tuo desktop e [Kaddressbook](/cloud/apps/contacts/desktop/kde-desktop-integration). 

Puoi utilizzare il tuo gestore di pacchetti per installare KMail se non è installato di default sulla tua distribuzione preferita.

Il client ti chiederà di aggiungere un account la prima volta che avvierai KMail.

Segui le istruzioni per inserire tutte le tue credenziali:

|![](en/kmail1.png)|Nome completo: ```Nome che verrà visualizzato nella sezione "Da:" ```<br>Email: ```tuo_username_@_disroot.org```<br>Password: ```la_tua_super_password```|
|:--:|--|

Fai clic su "**Avanti**" una volta terminato e verificato che tutto sia corretto.

La schermata successiva riguarda la sicurezza delle comunicazioni. Si consiglia di crittografare la comunicazione.
![](en/kmail2.png)

Clicca sul bottone  "**Prossimo**" una volta che hai fatto.

Nella schermata finale ti verrà mostrato che l'installazione è completata.

![](en/kmail3.png)

Clicca su "Finito" ed è **tutto** \o/.
