---
title: 'Desktop Clients'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

![](../thumb.png)

## Client di posta multipiattaforma
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)


## GNU/Linux
- [Mutt](mutt)


## GNU/Linux: integrazione col desktop
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)
