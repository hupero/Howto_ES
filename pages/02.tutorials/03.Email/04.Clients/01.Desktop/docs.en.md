---
title: 'Desktop Clients'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

![](../thumb.png)

## Multiplatform Mail Clients
- [Thunderbird](thunderbird)
- [Claws Mail](claws-mail)


## GNU/Linux
- [Mutt](mutt)


## GNU/Linux: Email desktop integration
- [GNOME](gnome-desktop-integration)
- [KDE](kde-desktop-integration)
