---
title: 'Cliente de desktop'
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - clients
        - desktop
page-toc:
    active: false
---

Como configurar o seu cliente de email no seu desktop:

## Table of content
- [Thunderbird - cliente de email multiplataforma](thunderbird)
- [Mutt - cliente de email para GNU/Linux](mutt)

![](c64.jpg)
