---
title: Clients Settings
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - settings
page-toc:
    active: false
---

# Server Settings
Most E-mail client will be able to discover the correct server setting automagically, but in some cases you may need to manually enter the following information to configure your email client.

**IMAP Server**: disroot.org <br>
**SSL Port**: 993 <br>
**Authentication**: Normal Password

**SMTP Server**: disroot.org <br>
**STARTTLS Port**: 587 <br>
**Authentication**: Normal Password

**SMTPS Server**: disroot.org <br>
**TLS Port**: 465 <br>
**Authentication**: Normal Password

**POP Server**: disroot.org <br>
**SSL Port**: 995 <br>
**Authentication**: Normal Password

---

#### Related how-tos:
- [**Webmail**](/tutorials/email/webmail)
- [**Desktop Clients**](/tutorials/email/clients/desktop)
- [**Mobile Clients**](/tutorials/email/clients/mobile)
