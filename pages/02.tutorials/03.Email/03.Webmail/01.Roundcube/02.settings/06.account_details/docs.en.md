---
title: "Account details"
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - webmail
page-toc:
    active: true
---

# Account details

![Account details](en/set_account.png)

Here you will find information about:
  - Your email user
  - Your system
  - Your mailbox
  - Roundcube version

You will also find a donate button if you want to contribute to **Roundcube Project**.
