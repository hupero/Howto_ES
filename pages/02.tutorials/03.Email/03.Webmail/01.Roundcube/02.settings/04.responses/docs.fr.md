---
title: "Webmail : Paramètres / Réponses"
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - webmail
page-toc:
    active: true
---

# Paramètres

## Réponses

Cette option vous permet d'enregistrer les réponses, ce qui peut être pratique lorsque vous écrivez plusieurs fois des e-mails avec le même texte.

**Note** : il s'agit uniquement d'extraits de texte qui peuvent être insérés n'importe où et non de modèles de messages avec objet, texte ou pièces jointes.

![Réponses](en/set_responses.png)

### Créer une réponse
Il existe deux façons de créer une réponse.

01. La première consiste à créer une réponse à partir de l'option **Réponses** dans les **Paramètres** en cliquant sur le bouton **Créer**.

![Réponses](en/create.gif)

Entrez un nom pour identifier votre réponse, le texte et enfin **Save**.

![Réponses](en/responses_edit.png)

Vous pouvez également modifier vos réponses à partir d'ici : leur nom, leur contenu, les supprimer, en ajouter de nouvelles.

02. La deuxième façon de créer une réponse pendant la composition d'un e-mail.

Allez dans **Composer** et cliquez sur le bouton **Réponses** dans la barre supérieure pour ajouter ou insérer une réponse à votre email.

![Réponses](en/responses.png)

Vous avez trois options ici :

- **Insérer une réponse** : Si vous avez configuré une ou plusieurs réponses, elles apparaîtront ici. Il suffit de cliquer dessus pour l'insérer dans votre message.

- **Créer une nouvelle réponse** : Cliquez sur cette option pour en créer une. La boîte de création s'ouvre, entrez un nom et le texte de votre réponse et enregistrez-la.

![Réponses](en/responses_new.png)

- **Editer les réponses** : En cliquant sur cette option, vous accéderez aux **Paramètres**, dans la section **Réponses** où vous pourrez modifier les réponses.
