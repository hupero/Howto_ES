---
title: Roundcube Migration Guide
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: false
---

# Guida migrazione a Roundcube

![](thumb.png)

A partire dal 16 luglio la soluzione webmail **Disroot** è **Roundcube** mentre **SnappyMail** non sarà più disponibile.

Fino al 1 settembre, entrambi i client webmail saranno disponibili per dare a tutti il tempo di effettuare la transizione, dopo tale data sarà accessibile solo Roundcube.

Sebbene non dovresti avere problemi con la transizione, ti consigliamo vivamente di eseguire in precedenza un backup dei tuoi contatti. Di seguito troverai una breve guida passo passo per aiutarti a farlo senza problemi.

Iniziamo...

----

# Indice
##  1. [FAQ migrazione](faq)
##  2. [Effettua il backup/esporta e importa i tuoi contatti](backup)
