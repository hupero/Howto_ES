---
title: FAQ
published: true
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
page-toc:
    active: true
---

# Roundcube Migration FAQ

## Where can I learn more about Roundcube?
We have a **Roundcube** howto that you can find [here](../../roundcube).

## Will I need to use different login credentials to access my email?
No, your **Disroot** credentials will be the same as always.

## Will I need to reconfigure my phone or desktop mail client?
No, this migration shouldn't affect your phone or email client setup.

## How will I access Roundcube?
In the same way that you usually access webmail.

## What will happen to my emails and contacts?
All your emails and contacts will be automatically transferred into **Roundcube**. Anyway, we strongly suggest you to backup your contacts. In this guide you can find how to do it.

## Can I switch back to the previous webmail?
No, you cannot.

## My contacts did not migrate, what should I do?
First of all, don't panic. If your contacts did not migrate, please contact us and we fix it.

**Finally, if you notice anything unusual, have questions or suggestions, please contact us at support**
