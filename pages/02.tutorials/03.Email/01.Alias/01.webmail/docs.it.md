---
title: Email Alias: Setup on webmail
published: true
visible: false
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
        - webmail
page-toc:
    active: true
---

# Imposta alias su Webmail
Per prima cosa, accedi alla tua webmail e vai alle impostazioni della tua posta (icona in basso a sinistra) 

![](en/settings1.png)

Quando sei in Impostazioni, vai alla scheda **"Identità"**, fai clic su "**Aggiungi un'identità"** e compila il modulo. Una volta terminato, premi il pulsante **"Aggiungi"**.

*(Ogni* **Disroot** *l'utente ha un* nome utente@disr.it *alias da utilizzare per impostazione predefinita)* 

![](en/identity_add.gif)

# Imposta come default
Puoi gestire l'identità predefinita semplicemente trascinando l'identità in cima all'elenco. 

![](en/identity_default.gif)

# Invia una email
Per inviare e-mail con il tuo nuovo alias, fai clic sul campo **"Da"** e seleziona l'alias che desideri utilizzare dal menu a discesa, durante la composizione della posta. 

![](en/identity_send.gif)
