---
title: Alias de correo: Configurar en el Webmail
published: true
visible: false
indexed: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - webmail
        - correo
        - alias
        - configuraciones
page-toc:
    active: true
---

## Configuración
Primero, accede a tu webmail y ve a tus configuraciones de correo (icono abajo a la izquierda)

![](en/settings1.png)

Cuando estés en Configuraciones, ve a la pestaña "**Identidades**", click en "**Agregar una Identidad**" y completa el formulario. Una vez hecho, presiona el botón "**Agregar**".
*(Todo usuario de disroot tiene por defecto un alias* usuario@disr.it *que puede utilizar)*

![](en/identity_add.gif)

## Establecer como predeterminado
Puedes administrar tu identidad predeterminada, simplemente arrastrándola hasta arriba de la lista.

![](en/identity_default.gif)

## Enviar correo
Para enviar un correo con tu nuevo alias, sólo haz click en el campo "**De**" y selecciona el alias que desees utilizar desde el menú desplegable, cuando estés redactando un correo.

![](en/identity_send.gif)
