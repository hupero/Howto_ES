---
title: 'Alias setup'
visible: true
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - settings
page-toc:
    active: false
---

# Alias di posta elettronica

Dopo aver richiesto gli alias email utilizzando questo [modulo](https://disroot.org/en/forms/alias-request-form), dovrai configurarli. Di seguito puoi trovare come farlo su vari client di posta elettronica. 

- [Webmail](webmail)
- [Thunderbird](thunderbird)
- [K9](k9)
- [FairEmail](fairemail)
- [Mail iOS](mailios)
