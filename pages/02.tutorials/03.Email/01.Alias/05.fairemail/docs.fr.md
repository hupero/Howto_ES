---
title: Alias d'email : Configuration sur FairEmail
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
        - fairemail
        - android
page-toc:
    active: true
---

# Alias sur FairEmail

Tout d'abord, vous ouvrez **FairEmail** et allez dans "Créer une identité" dans les paramètres principaux, point 2, en tapant sur le bouton "Gérer" (3 points en haut à gauche '>' Paramètres '>' onglet "paramètres principaux").

![](en/fairemail_alias_01.png)

Dans les paramètres, vous taperez et maintiendrez votre **Disroot**-Compte jusqu'à ce qu'un menu s'ouvre. Dans ce menu, tu choisiras *Copier...*.

![](en/fairemail_alias_02.png)

Dans le dialogue de saisie suivant, on vous présente une identité, qui est connectée à votre compte **Disroot** existant. Remplissez vos données ou modifiez les informations remplies automatiquement comme vous le souhaitez.

*(Chaque* **Disroot** *utilisateur a un* username@disr.it *alias à utiliser par défaut)*.

![](en/fairemail_alias_03.png)

Terminez votre saisie en tapant sur le bouton *Save* en bas du menu. Votre ALias est maintenant configuré.

![](en/fairemail_alias_04.png) ![](en/fairemail_alias_05.png)

# Créer un email
Pour envoyer un email avec votre nouvel alias, tapez sur le champ *From:* et choisissez l'alias que vous voulez utiliser dans le menu déroulant qui apparaît.

![](en/fairemail_alias_06.png)
