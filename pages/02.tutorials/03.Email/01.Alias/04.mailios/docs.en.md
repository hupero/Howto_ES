---
title: 'Email Alias: Setup on Mail iOS'
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - email
        - alias
page-toc:
    active: true
---

# Configurer un alias dans Mail iOS

Tout d'abord, ouvrez **Paramètres** et allez dans la section **Mot de passe et comptes**.

![](en/identity_settings.png)

Une fois dans **Paramètres**, sélectionnez **Ajouter un nouveau compte**...

![](en/identity_settings2.png)

... puis **Autres**...

![](en/identity_settings3.png)

... et enfin, choisissez **Add Mail Account**.

![](en/identity_settings4.png)
<br>

Sur l'écran **Nouveau compte**, remplissez les détails de votre compte alias (nom, nom d'utilisateur, mot de passe et description du compte), puis cliquez sur **Suivant**.

![](en/identity_settings5.png)

L'écran suivant vous demandera de remplir les détails des serveurs **IMAP** et **SMTP**. Leave the first fields just like they appear.

![](en/identity_settings6.png)

Dans **Serveur de courrier entrant** ainsi que **Serveur de courrier sortant**, indiquez le nom d'utilisateur de votre compte de base (pas l'alias).

![](en/identity_settings7.png)

!! ATTENTION !

!! Pour **IMAP** et **SMTP**, tous les détails doivent être remplis (même si cela dit "facultatif").

Cliquez ensuite sur **Suivant**.

À partir de maintenant, lorsque vous envoyez un e-mail, vous pouvez sélectionner votre alias en appuyant sur le champ "*From*" et en le choisissant.

![](en/identity_settings8.png)
