---
title: 'Учётная запись'
updated:
published: true
visible: true
indexed: 
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - administration
page-toc:
  active: false
---

# Управление учётной записью

## [Управление учётной записью](administration)
Изменить или сбросить пароли, уведомления, контрольные вопросы, формы доступа или удалить учётную запись.

## [Запрос псевдонима](alias-request)
Как запросить псевдонимы электронной почты.