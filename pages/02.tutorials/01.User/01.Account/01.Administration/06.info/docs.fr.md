---
title: 'Informaiton du compte'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - compte
        - information
page-toc:
    active:
---

# Informations sur votre compte

![](en/dashboard_info.png)

Vous trouverez ici un résumé des **informations sur le compte et le mot de passe**, **la politique de mot de passe** (les règles auxquelles un mot de passe doit se conformer) et l'**historique** de la gestion des mots de passe.

![](en/account.png)

Cette section est purement informative.
