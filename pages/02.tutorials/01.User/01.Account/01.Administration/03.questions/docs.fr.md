---
title: 'Questions de sécurité'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - compte
        - gestion
page-toc:
    active:
---

# Configurez vos questions de sécurité

![](en/dashboard_questions.png)

En cas d'oubli/de perte de votre mot de passe, vous pouvez le réinitialiser sans l'intervention des administrateurs en configurant d'abord les questions de sécurité. Pour ce faire, cliquez sur cette option.

Le processus est assez simple.

- Cliquez sur ***Setup Security Questions***.

 ![](en/sec_qs_01.png)

- Rédigez la première question et sa réponse, puis sélectionnez les deux questions suivantes dans la liste déroulante et rédigez également les réponses.

  ![](en/sec_qs_02.png)

- Une fois que les réponses répondent aux exigences, cliquez simplement sur **Sauvegarder les réponses** et enfin **Continuer**.

  ![](en/sec_qs_03.png)
