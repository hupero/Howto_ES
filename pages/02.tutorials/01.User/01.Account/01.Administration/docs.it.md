---
title: 'Manage your Disroot Account'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - management
page-toc:
    active: true
---

# [Pannello di controllo](ussc)
- Panoramica
- [Reimposta la password](ussc/pwd_reset)
- [Registrazione nuovo account](ussc/new_reg)

## [Cambia la password](password)
Aggiorna la tua chiave crittografica del **Cloud**

# [Imposta le domande di sicurezza](questions)

## [Aggiorna il tuo profilo](profile)

## [Moduli di richiesta](forms)

## [Infomrazioni sull'account](info)

## [Elimina l'account](delete)
