---
title: 'Suppression du compte'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - deletion
page-toc:
    active:
---

# Supprimer votre compte
Si pour une raison quelconque vous souhaitez supprimer votre compte, cliquez simplement sur cette option.

![](en/dashboard_delete.png)

Une fois que vous êtes sûr de vouloir supprimer le compte, cochez la case **J'accepte** et enfin **Supprimer**.

![](en/delete.png)

!! #### NOTE<br>
!! **Ce processus est irréversible. Une fois confirmé, vous ne pourrez plus vous connecter à votre compte ou demander à le restaurer ultérieurement.**<br>
!! **Toutes vos données restantes seront supprimées dans les 48 heures, et votre nom d'utilisateur actuel ne sera pas disponible lors de la création d'un nouveau compte.**.
