---
title: 'Email alias request'
published: true
indexed: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - alias
page-toc:
    active: false
---

# Come richiedere un alias email
La funzione di alias e-mail viene offerta come "ricompensa" per i sostenitori regolari. Per sostenitori assidui pensiamo a chi contribuisce con almeno l'equivalente di una tazzina di caffè al mese. Affinché la "ricompensa" non diventi l'unico incentivo alla donazione, ci aspettiamo di vedere la donazione effettuata prima di poter abilitare questa funzione. 

Non è che stiamo promuovendo il caffè (che è, in realtà, un simbolo molto utile per [sfruttamento e disuguaglianza](http://www.foodispower.org/coffee/)). Abbiamo pensato che fosse un buon modo per permettere alle persone di misurare da sole quanto possono dare. 

**Per favore prenditi del tempo per considerare il tuo contributo.** Se puoi "comprare" una tazza di **Rio De Janeiro** caffè al mese va bene, ma se puoi permetterti un *Frappuccino di soia doppio decaffeinato con un colpo extra E Cream* al mese, puoi davvero aiutarci a mantenere in funzione la piattaforma **Disroot** e assicurarti che sia disponibile gratuitamente per altre persone con meno mezzi. 

Abbiamo trovato questo [elenco](https://www.caffesociety.co.uk/blog/the-cheapest-cities-in-the-world-for-a-cup-of-coffee) di prezzi delle tazze di caffè in tutto il mondo , potrebbe non essere molto preciso, ma fornisce una buona indicazione delle diverse tariffe. 

Quindi, **per richiedere un alias** devi compilare questo [modulo](https://disroot.org/forms/alias-request-form). 

Ora, se stai cercando **come impostare un alias email**, dai un'occhiata a [questo tutorial](/tutorials/email/alias).
