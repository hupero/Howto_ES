---
title: 'Account'
updated:
published: true
visible: true
indexed: 
taxonomy:
    category:
        - docs
    tags:
        - user
        - account
        - administration
page-toc:
  active: false
---

# Gestione account

## [Amministrazione](administration)
Modifica o reimposta password, modifica notifiche o reimposta e-mail, domande di sicurezza, moduli di accesso, elimina account. 

## [Richiesta alias](alias-request)
Come richiedere alias email. 
