---
title: "Discourse: Exporting your Forum posts"
published: true
indexed: true
updated:
    last_modified: "February 2022"		
    app: Discourse
    app_version: 2.3
taxonomy:
    category:
        - docs
    tags:
        - user
        - forum
        - discourse
        - gdpr
visible: true
page-toc:
    active: false
---

**Discourse**, il software utilizzato da **Disroot** per il **Forum**, ti consente di esportare il contenuto del testo da tutti i tuoi post in un file .csv (che è supportato dalla maggior parte dei fogli di calcolo (Libreoffice, Openoffice, Gnumeric, Excel).

**Per esportare i tuoi post da Discourse:**
- Accedi al **Forum**
- Premi il tuo avatar utente nell'angolo in alto a destra dello schermo
- Premi il pulsante con il tuo nome utente 

![](en/export.gif)

- Premi il bottone **Scarica tutto**

  ![](en/download_1.png)

- Apparirà una finestra pop-up che ti chiederà se desideri scaricare i tuoi post, quindi premi **OK**

  ![](en/download_2.png)

- Il sistema inizierà a elaborare i tuoi dati e ti invierà una notifica quando sarà pronto per il download. 

  ![](en/download_3.png)

  ![](en/notification.png)

- Riceverai un messaggio dal sistema che ti informa che i dati sono pronti per essere scaricati e ti fornisce un link per scaricare il file .csv con una copia dei tuoi post. Se hai abilitato le notifiche via e-mail, riceverai anche un'e-mail con queste informazioni. 

- Premi il link per scaricare il file. 

  ![](en/notification_2.png)  

- Il link sarà disponibile per 48h, dopodiché scadrà e dovrai esportare nuovamente i tuoi dati. 

- Una volta estratto il file, puoi aprirlo con un programma di fogli di calcolo.


**ATTENZIONE**: I dati possono essere scaricati solo una volta ogni 24h 
