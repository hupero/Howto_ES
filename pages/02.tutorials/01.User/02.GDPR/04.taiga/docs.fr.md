---
title: "Taiga: Exportez vos Projets"
published: true
visible: true
indexed: true
updated:
    last_modified: "Juin 2020"		
    app: Taiga
    app_version: 4.0.0
taxonomy:
    category:
        - docs
    tags:
        - utilisateur
        - taiga
        - rgpd
    visible: true
page-toc:
    active: false
---

Pour exporter les données de votre projet depuis **Taiga**, il suffit de suivre les étapes suivantes :

- Allez sur https://board.disroot.org et connectez-vous.
- Sélectionnez le projet que vous voulez exporter et allez dans les paramètres de l'administrateur.
- Sous l'onglet **Projet**, vous trouverez l'option **Export**. Cliquez sur le bouton **EXPORT** pour générer un fichier .json contenant toutes les informations du projet. Vous pouvez l'utiliser comme sauvegarde ou pour lancer un nouveau projet basé sur ce fichier.

![](en/export.gif)
