---
title: "Taiga: Exportar proyecto"
published: true
visible: true
indexed: true
updated:
    last_modified: Junio, 2020		
    app: Taiga
    app_version: 4.0.0
taxonomy:
    category:
        - docs
    tags:
        - usuarix
        - taiga
        - rgpd
    visible: true
page-toc:
    active: false
---

Sigue los pasos a continuación para exportar la información de tu proyecto desde la interfaz de **Taiga**:

- Ve a [https://board.disroot.org](https://board.disroot.org) e inicia sesión.
- Selecciona el proyecto que quieres exportar y ve a las configuraciones de Admin (abajo en la barra lateral izquierda).
- Dentro de la pestaña **Proyecto** encontrarás la opción **Exportar**. Haz click en el botón **EXPORTAR** para general un archivo .json con toda la información del proyecto. Puedes utilizarlo como un respaldo o iniciar un nuevo proyecto basado en él.

![](en/export.gif)
