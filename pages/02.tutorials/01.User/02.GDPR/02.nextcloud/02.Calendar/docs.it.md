---
title: 'Nextcloud: Exporting Calendars'
published: true
indexed: true
updated:
    last_modified: "February 2022"		
    app: Nextcloud
    app_version: 18
taxonomy:
    category:
        - docs
    tags:
        - user
        - cloud
visible: true
page-toc:
    active: false
---

Per esportare i calendari seguire i seguenti passi:

1. Accedi al [cloud](https://cloud.disroot.org)

2. Seleziona l'applicazione calendario

![](en/select_app.gif)

3. Esporta i tuoi calendari o i calendari che hai sottoscritto.
Seleziona i *"tre puntini"* nel menu opzione a fianco del calendario che vuoi esportare e clicca sull'opzione *"Esporta"*. I calendari esportati saranno salvati nel formato .ics.

![](en/export-calendar.gif)

Ripeti la procedura per tutti i calendari che vuoi esportare.
