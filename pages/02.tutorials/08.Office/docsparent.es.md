---
title: Oficina
subtitle: "Blocks de notas, Pastebin, Mumble, Encuestas & Compartir archivos"
icon: fa-file-text
published: true
taxonomy:
    category:
        - docs
        - topic
    tags:
        - oficina
        - cryptpad
        - pastebin
        - archivos
        - compartir
        - mumble
        - disapp
page-toc:
    active: false
---

# Herramientas de Oficina

**Disroot** proporciona un conjunto de herramientas web que tal vez quieras probar.

---

Para sacarles mayor provecho, hay también una aplicación de **Disroot** que reúne todas estas herramientas y otros servicios:

### [DisApp](../user/disapp)
