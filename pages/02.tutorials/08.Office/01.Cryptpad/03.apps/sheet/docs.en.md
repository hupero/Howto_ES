---
title: Sheet
published: true
visible: false
indexed: true
updated:
        last_modified: "September 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - sheet
        - spreadsheets
        - applications
visible: true
page-toc:
    active: false
---

# Sheet

Create and edit spreadsheets.
