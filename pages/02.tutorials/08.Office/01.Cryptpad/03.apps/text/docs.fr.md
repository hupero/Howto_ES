---
title: Texte enrichi
published: true
visible: false
indexed: true
updated:
        last_modified: "Septembre 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - texte enrichi
        - éditeur de texte
        - applications
visible: true
page-toc:
    active: false
---

# Texte enrichi

Éditeur de texte enrichi pour créer, composer, modifier et mettre en forme des documents texte.
