---
title: Rich Text
published: true
visible: false
indexed: true
updated:
        last_modified: "September 2021"
taxonomy:
    category:
        - docs
    tags:
        - cryptpad
        - rich text
        - text editor
        - applications
visible: true
page-toc:
    active: false
---

# Rich Text

Rich text editor to create, compose, edit and format text documents.
