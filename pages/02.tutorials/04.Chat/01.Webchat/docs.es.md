---
title: 'Chat: Webchat'
published: true
visible: true
indexed: true
updated:
        last_modified: "Abril 2019"
        app: Converse.js
        app_version: 4.2.0
taxonomy:
    category:
        - docs
    tags:
        - chat
        - xmpp
page-toc:
    active: true
---

## Converse.js
<br>
