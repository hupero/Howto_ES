---
title: 'Chat Clients: Android'
published: false
visible: false
taxonomy:
    category:
        - docs
    tags:
        - chat
        - xmpp
        - android
page-toc:
    active: false
---

How to setup your chat client on your mobile:

## Clients
- [Conversation](conversation)
