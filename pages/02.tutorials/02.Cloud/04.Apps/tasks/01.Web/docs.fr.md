---
title: Interfac web
published: true
visible: false
updated:
        last_modified: "Janvier 2021"
        app: Tâches
        app_version: 0.13.6
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - sync
page-toc:
    active: false
---

# Tâches

![](en/main.png)

L'application **Tâches** vous permet d'ajouter et de supprimer des tâches, de modifier leur titre, leur description, leur date de début et d'échéance et de les marquer comme importantes. Une tâche peut être le rappel d'une date de réunion, un travail à faire, une activité personnelle ou de groupe et bien d'autres choses encore.

Dans ce petit guide, nous allons voir comment cela fonctionne.

# Ajouter une nouvelle tâche
Pour commencer, cliquez sur **+ Add list...**, écrivez le nom de votre nouvelle liste de tâches et appuyez sur Entrée.

![](en/add_task.gif)

Une fois que tu l'as fait, une nouvelle section apparaîtra à droite.

![](en/add_tasks_window.png)

Tapez le nom de la nouvelle tâche que vous voulez ajouter, puis appuyez sur la touche Entrée. Cliquez sur l'icône à trois points à droite si vous voulez ajouter des sous-tâches.

![](en/add_tasks.gif)

## Options des tâches
Cliquez sur le titre de la tâche ou de la sous-tâche pour accéder aux options.

![](en/tasks_options.png)

Ici, vous pouvez :

- **Définir la date de début et la date d'échéance**<br>.
En cliquant sur l'option Début/Date d'échéance, vous pouvez définir le jour et l'heure de celle-ci.

  ![](en/start_due_date.gif)

Vous pouvez également définir la tâche comme une activité de toute la journée.

  ![](en/all_day.png)

- **Assigner / modifier la liste des tâches**<br>
Vous pouvez modifier / affecter une tâche ou une sous-tâche à différentes listes ou calendrier. Il suffit d'en sélectionner une dans le menu déroulant et les tâches passeront à la nouvelle liste.

  ![](en/assign_list.gif)

- **Sélectionnez une classification**<br>
Vous pouvez affecter la liste des tâches à un calendrier précédemment créé et choisir la manière dont elle doit être affichée.

  ![](en/show.png)

- **Sélectionnez un état**<br>
Sélectionnez si la tâche **nécessite une action**, si elle est **en cours**, **complétée** ou **annulée**.

  ![](en/status.gif)

- **Assigner un niveau de priorité à la tâche**.
En déplaçant la barre, vous pouvez définir le niveau de priorité où 1 à 4 est **Haut**, 5 est **Moyen** et 6 à 9 est **Bas**.

  ![](en/priority.gif)

- **Définir et modifier le niveau de progression de la tâche**<br>
En déplaçant la barre, vous pouvez régler la progression de la tâche de 0% à 100%.

  ![](en/progress.gif)

- **Catégories et commentaires**<br>
L'attribution de catégories et l'ajout de commentaires peuvent être très utiles lorsque vous travaillez en groupe. Cliquez sur **Sélectionner les catégories**, sélectionnez ou créez-en une et appuyez sur Entrée. Pour écrire un commentaire, il suffit de le taper dans la case.

  ![](en/categories_comments.gif)

  Lorsque la tâche a une catégorie assignée ou un commentaire ajouté, vous pouvez le voir dans la description, à côté du titre.

  ![](en/categories_comments.png)

- Enfin, en bas des détails de la tâche, vous verrez une barre avec les options de suppression et d'information.

  ![](en/info.png)
