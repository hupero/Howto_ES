---
title: "Cloud App: Attività"
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - task
        - sync
    visible: true
page-toc:
    active: false
---

# Tasks
L'app **Attività** ti consente di aggiungere, modificare ed eliminare attività nel tuo **Cloud**. Possono essere condivisi tra utenti, sincronizzati utilizzando [**CalDav**](https://en.wikipedia.org/wiki/CalDAV) per sincronizzarli con il tuo client locale e persino scaricati come [ICS](https:// en.wikipedia.org/wiki/ICalendar). 

### [Interfaccia web](web)
- Creare e configurare attività

### [Client desktop](desktop)
- Client desktop e applicazioni per l'organizzazione e la sincronizzazione di attività

### [Client mobile](mobile)
- Client mobile e impostazioni per l'organizzazione e la sincronizzazione delle attività
