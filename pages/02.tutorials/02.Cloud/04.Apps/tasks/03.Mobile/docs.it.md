---
title: Mobile
published: true
visible: false
updated:
        last_modified: "July 2019"
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - mobile
page-toc:
    active: false
---

# Integrazione dei task nei dispositivi mobili

Per configurare e sincronizzare le tue **Attività** tramite un client mobile, consulta il tutorial di seguito: 

## Android
- [DAVx⁵ / OpenTasks](/tutorials/cloud/clients/mobile/android/calendars-contacts-and-tasks)
- [App mobile per Nextcloud](/tutorials/cloud/clients/mobile/android/nextcloud-app)
