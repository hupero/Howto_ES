---
title: Desktop
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - task
        - cloud
        - sync
page-toc:
    active: false
---

# Tasks nel desktop

Esistono diversi modi per sincronizzare e lavorare con **Attività Nextcloud** dal desktop. Di seguito troverai alcuni tutorial per ottenerlo. 

## Desktop client multipiattaforma
 - [Thunderbird: Calendario / Contatti / Tasks sincronizzati](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)

## GNU/Linux
 - [GNOME](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
 - [KDE](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
