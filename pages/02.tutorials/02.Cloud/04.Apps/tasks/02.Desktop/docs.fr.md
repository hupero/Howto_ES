---
title: Bureau
published: true
visible: false
updated:
taxonomy:
    category:
        - docs
    tags:
        - tâche
        - cloud
        - sync
page-toc:
    active: false
---

# Tâches sur le bureau

Il existe plusieurs façons de synchroniser et de travailler avec **Tâches Nextcloud** depuis votre bureau. Vous trouverez ci-dessous quelques tutoriels pour y parvenir.

## Clients de bureau multi-plateformes
 - [Thunderbird : Calendrier / Contacts / Tâches](/tutorials/cloud/clients/desktop/multiplatform/thunderbird-calendar-contacts)

## GNU/Linux
 - [GNOME Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/gnome-desktop-integration)
 - [KDE Desktop Integration](/tutorials/cloud/clients/desktop/gnu-linux/kde-desktop-integration)
