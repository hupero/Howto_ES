---
title: Bookmarks: Web
published: true
visible: false
updated:
        last_modified: "febbraio 2022"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - bookmarks
page-toc:
    active: false
---

## Segnalibri
