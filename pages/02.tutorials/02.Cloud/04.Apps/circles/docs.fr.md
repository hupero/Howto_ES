---
title: Cercles
published: true
visible: false
indexed: true
updated:
        last_modified: " 2021"
        app: Circles
        app_version: 0.20.6
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - cercles
visible: true
page-toc:
    active: false
---

# Cercles (à venir)

**Circles** vous permet de créer vos propres groupes d'utilisateurs/collègues/amis. Ces groupes (ou "cercles") peuvent ensuite être utilisés par n'importe quelle autre application à des fins de partage (fichiers, flux social, mise à jour de statut, messagerie, etc.)
