---
title: Nube - Apps: Notas"
published: true
visible: false
updated:
indexed: true
taxonomy:
    category:
        - docs
    tags:
        - nube
        - notas
visible: true
page-toc:
    active: false
---
## Notas

### [Interfaz web](web)
- Crear y editar notas

### [Cliente de Escritorio](desktop)
- Clientes para el escritorio de la aplicación Notas

### [Clientes para móvil](mobile)
- Aplicación Notas, DAVx⁵, configuraciones de dispositivos
