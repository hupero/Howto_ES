---
title: Notes: Mobile apps
published: true
visible: false
updated:
        last_modified: "July 2019"
        app:
        app_version:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
        - android
        - mobile
page-toc:
    active: false
---

## Android
#### [Notes App](android)

----
### Related How-Tos
#### [Nexcloud mobile app](/tutorials/cloud/clients/mobile/android/nextcloud-app)
