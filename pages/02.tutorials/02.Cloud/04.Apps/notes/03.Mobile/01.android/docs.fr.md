---
title: Notes: Portable
published: true
visible: false
updated:
        last_modified: "Avril 2019"
        app: Nextcloud-Notes
        app_version: 0.24.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - notes
page-toc:
    active: false
---

# Notes sur portable

Dans ce tutoriel, nous allons vous montrer comment configurer et utiliser vos notes Disroot dans votre appareil Android.


# Installer l'application Notes

Tout d'abord, prenez **"Nextcloud Notes "** de [F-Droid](https://f-droid.org/packages/it.niedermann.owncloud.notes/) ou d'autres magasins d'applications commerciales.    



# Ajoutez votre compte disroot

Une fois que vous lancez l'application *"Notes "*, pour la première fois, vous devrez ajouter vos informations d'identification et l'URL du serveur cloud disroot.


- Adresse du serveur:** [https://cloud.disroot.org/](https://cloud.disroot.org/)
- **Nom d'utilisateur:** Votre nom d'utilisateur disroot
- **Password:** Votre mot de passe disroot

Après cela, appuyez simplement sur "*connect*".
Vos notes existantes de votre compte cloud disroot devraient maintenant être synchronisées avec votre téléphone.

![](en/nextcloud_notes1.png)


# Créer et supprimer des notes

Vous pouvez créer une nouvelle note en appuyant sur le bouton *"plus "* (**+**) dans le coin inférieur droit de l'écran.

![](en/nextcloud_notes2.png)

L'éditeur est très simple :

* la première ligne de la note est automatiquement le titre de la note.
* Après avoir écrit votre note, appuyez sur la flèche arrière et l'application enregistrera automatiquement la note.
* Vous aurez quelques options en appuyant sur le menu à trois points en haut à droite :
  **Favoris:** cochez cette case si vous voulez ajouter la note à vos favoris.
  **Catégorie:** Option pour classer la note dans une catégorie existante ou nouvelle.
  **Share:** Pour partager votre note
  **Cancel:** Pour annuler votre dernière modification
  **Delete:** Pour supprimer la note
* Tout changement apporté aux notes sera automatiquement synchronisé vers et depuis le nuage de disroot.

L'application Notes utilise le formatage Markdown, donc si vous êtes familier avec ce format, vous pouvez leur donner une belle apparence. Si vous ne savez toujours pas ce qu'est le format Markdown, vous devez absolument consulter [cette page] (http://lifehacker.com/5943320/what-is-markdown-and-why-is-it-better-for-my-to-do-lists-and-notes) qui changera votre vie pour toujours :)

![](en/nextcloud_notes1.gif)

Toutes les notes que vous créez peuvent être vues dans le menu principal de l'application Nextcloud Notes.

* Pour éditer une note particulière, il suffit de cliquer dessus.
* Pour créer de nouvelles notes, appuyez simplement sur le symbole plus.


![](en/nextcloud_notes3.png)


# Parcourir les catégories de vos notes

Si vous avez classé votre note en catégories, vous pouvez facilement retrouver vos notes en parcourant les catégories.
Pour ce faire, vous devez taper sur le menu hamburger en haut à gauche.

![](en/nextcloud_notes4.png)

Une barre latérale avec toutes vos catégories apparaîtra.
Sélectionnez la catégorie et toutes les notes de cette catégorie seront listées.

![](en/nextcloud_notes5.png)


! ! **Note!** Toute modification apportée à vos notes dans l'application Notes, les notes dans l'interface web Notes, les notes dans le client de bureau ou dans le fichier . txt, apparaîtra dans tous les clients et l'interface web.
