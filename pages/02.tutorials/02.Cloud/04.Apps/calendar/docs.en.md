---
title: Calendar
published: true
visible: false
indexed: true
updated:
        last_modified: "December 2020"
        app: Calendar
        app_version: 2.1.3
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - app
        - calendar
page-toc:
    active: false
---

# Calendar
The **Calendar** app is the user interface for **Disroot**'s CalDAV server. It allows you to sync events from various devices with your **Disroot Cloud** and edit them online.

---

## [Web interface](web)
- Creating and configuring calendars

## [Desktop clients](desktop)
- Desktop clients and integration settings for organizing and synchronizing calendars

## [Mobile clients](/tutorials/cloud/clients/mobile)
- Mobile clients and settings for organizing and synchronizing calendars
