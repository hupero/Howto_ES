---
title: "Contacts: Web"
published: true
indexed: true
visible: false
updated:
        last_modified: "July 2019"
        app: Nextcloud
        app_version: 15
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - contacts
page-toc:
     active: true
---

# Contacts
You can access the app by pressing the contacts icon ![](en/contacts_top_icon.png) on the top bar in **Nextcloud**.


## Create a contact

In the contacts app select "*New contact*"

![](en/contacts_add1.png)

You will prompted with a form on the "*right-bar*" side to create the new contact.

![](en/contacts_add2.png)

Just type the information you want/have in the fields. If you need, you can just add more fields at the bottom of the form.

![](en/contacts_add3.png)


## Delete a contact

* Select the contact
* In the contact form header, select the delete icon

![](en/contacts_delete.png)


## Create contact groups
You can create groups to organize your contacts ex: faculty, work, collective, etc.
In the field group you can assign a new contact to an existing group or create a new group. Or assign a contact to multiple groups by typing the several groups.

![](en/contacts_groups1.png)

On the left side of the screen in your contacts app, you will see the existing groups.
Selecting them will present all contacts in that respective group.

![](en/contacts_groups2.png)


## Share address books

Go to "settings" on the lower left corner of the screen in the contacts app.

![](en/contacts_share1.png)

In settings you can share your address book with other **Disroot** users by:<br>
 - Selecting share address book
 - Writing the username of the **Disroot** user you want to share the address book with.

![](en/contacts_share2.png)

You can also use a link to share your address book via **webDAV**, to other contact books (**Thunderbird**, mobile, etc,).

![](en/contacts_share3.png)


## Import address books

You can import address books or individual contacts, if you have a vcf file of the contact or the address book.

* Selecting "Import".

![](en/contacts_import1.png)

Then select the file you want to import, and press ok.


## Create a new address book

Inside settings in the field "Address book name" write the name of the new address book, then press enter.

![](en/contacts_create1.png)


# Syncing cloud contacts with webmail
Syncing cloud contacts with Webmail is very easy. It will permit contacts from your webmail and cloud to be in sync.

First go to your **Nexcloud** contacts app. Click on the settings icon in the lower left corner.
Select "Copy link" option of the addressbook you would like to sync with webmail, the address will be copied to the clipboard.

![](en/webmail_contact_export.png)


Now go to **Mail** app, and click on the settings icon (top right in the webmail app)

![](en/webmail_contact_export_2.png)

In your settings, on the left side bar select **Contacts** and once there:

  1. Select Enable remote synchronization
  2. In Addressbook URL, paste the URL from your Cloud contacts addressbook you have saved before.
  3. Provide your username
  4. Add your password

![](en/webmail_contact_export_3.png)

And then refresh both pages. Now your contacts will stay in sync.
