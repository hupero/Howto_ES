---
title: Contacts
published: true
visible: false
indexed: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - apps
        - contacts
visible: true
page-toc:
    active: false
---

# Contatti
L'app **Contatti** ti consente di sincronizzare i contatti da vari dispositivi con il tuo **Disroot Cloud** e di modificarli online.

---

### [Interfaccia web](web)
- Creazione, modifica e sincronizzazione dei contatti

### [Clienti desktop](desktop)
- Client desktop e impostazioni di integrazione per l'organizzazione e la sincronizzazione dei contatti

### [Clienti mobili](/tutorials/cloud/clients/mobile)
- Client mobili e impostazioni per l'organizzazione e la sincronizzazione dei contatti
