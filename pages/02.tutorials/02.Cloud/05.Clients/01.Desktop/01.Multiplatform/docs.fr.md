---
title: Multiplatformes
published: true
indexed:
updated:
taxonomy:
    category:
        - docs
    tags:
        - cloud
        - clients
        - multiplatformes
visible: true
page-toc:
    active: false
---

## Clients multiplateformes


## [Client Nextcloud](desktop-sync-client)
- Client de synchronisation de bureau

## [Thunderbird](thunderbird-calendar-contacts)
- Synchronisation du calendrier, des contacts et des tâches

## [calcurse](calcurse-caldav)
- Synchronisation du calendrier en ligne de commande
