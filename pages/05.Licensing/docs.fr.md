---
title: How-to: License
published: true
visible: true
updated:
taxonomy:
    category:
        - docs
    tags:
        - licence
page-toc:
    active: false
---

# Licences des didacticiels
![](en/copyleft.png)

Toute la documentation du site How-to est sous une <br><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 License Internationale</a><br><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/80x15.png"/>
<br>

Nous pensons que les connaissances doivent être partagées et, pour cette raison, nous avons décidé de rendre tous les documents de **Disroot Howto** disponibles pour être utilisés par d'autres et améliorés ou adaptés à leurs besoins.<br>
Nous pensons également que la documentation que nous écrivons sur cette page concernant l'utilisation de nos services pourrait être utile à d'autres fournisseurs de services **Logiciels libres et open source** et/ou à des personnes qui utilisent les mêmes logiciels et/ou partagent les mêmes choix éthiques que **Disroot**.

En utilisant une **licence Creative Commons**, nous indiquons publiquement que toutes les documentations (how-to's) publiées sur [howto.disroot.org](https://howto.disroot.org) sont libres d'être adaptées, améliorées, distribuées et/ou partagées.

**Par conséquent, nous demandons à toutes les personnes qui souhaitent contribuer avec un nouveau tutoriel à notre page [How-to](https://howto.disroot.org), d'accepter de le publier sous la licence Creative Commons BY-SA**.

Le **Creative Commons BY-SA**, permet à quiconque de modifier et de partager librement un tutoriel dès lors que les conditions de publication sont conformes à ce qui suit :
- Reconnaissance de l'adaptation et des références à l'œuvre originale et/ou à l'auteur.
- Partage de la version modifiée avec la même licence (BY-SA), permettant aux autres de partager et de modifier l'œuvre également.


Vous pouvez en savoir plus sur le concept de **Copyleft** [ici].(https://en.wikipedia.org/wiki/Copyleft).

Et plus d'informations sur les **Licences Creative Commons** [ici].(https://creativecommons.org/).
